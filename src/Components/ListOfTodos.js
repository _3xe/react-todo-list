import React from 'react';
//Components
import Todo from './Todo';

const ListOfTodos = ({ todos, setTodos, filteredTodos}) => {
  return (
    <div className="todo-container">
      <ul className="todo-list">
        {filteredTodos.map((todo) => (
          <Todo 
            todos={todos}
            todo={todo} 
            setTodos={setTodos}
            key={todo.id} 
            text={todo.text} 
          />
        ))}
      </ul>
    </div>
  )
}

export default ListOfTodos;